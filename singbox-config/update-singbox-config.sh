#!/bin/sh

# tv live cn ip: 20.253.0.0/22

prefix_dir="/data/local/etc/singbox"
work_dir="/tmp"

#convserver="http://sub.jwsc.eu.org"
#convserver="https://url.v1.mk"
convserver="https://api.2c.lol"

crash_pid="$(pidof CrashCore)"

cd "$work_dir" && \
mkdir -p sb-tmp && cd sb-tmp && \
[ -n "$crash_pid" ] && ln -sf "/proc/${crash_pid}/exe" sing-box && \
curl --max-time 10 -o sub.json \
     "${convserver}/sub?target=singbox&insert=false&emoji=false&list=false&xudp=false&udp=false&tfo=false&expand=false&scv=true&fdn=false&url=https%3a%2f%2fknjc.cfd%2fapi%2fv1%2fclient%2fsubscribe%3ftoken%3d3870a715c186351033c95f5fe6947a21|https%3a%2f%2fsub3.smallstrawberry.com%2fapi%2fv1%2fclient%2fsubscribe%3ftoken%3ddo9qzecas3uas1bofa1plw5sx2j2h7ui&config=https%3a%2f%2fgitlab.com%2fsilverzhaojr%2fmess%2f-%2fraw%2fmain%2fsingbox-config%2fconfig.ini&exclude=25" && \
./sing-box format -c sub.json | sed -n '/"outbounds":/,/^  "[a-z]/p' | sed '$d' > o.json && \
echo '{' > outbounds.json && sed  's/^  ],$/  ]/' o.json >> outbounds.json && echo '}' >> outbounds.json && \
busybox patch -i "${prefix_dir}/outbounds.patch" && \
./sing-box merge config.json -c "${prefix_dir}/base.json" -c outbounds.json && \
./sing-box check -c config.json && \
mv /data/ShellCrash/jsons/config.json /data/ShellCrash/jsons/config.json.bak && \
cp config.json /data/ShellCrash/jsons && \
echo done && \
exit 0

echo failed && \
exit 1
