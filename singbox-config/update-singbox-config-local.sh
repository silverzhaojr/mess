#!/bin/sh

prefix_dir="/Users/zhaojr/Downloads/ax6000/singbox/"
work_dir="/tmp"

# server="http://sub.jwsc.eu.org"
server="https://url.v1.mk"

suburl="https://knjc.cfd/api/v1/client/subscribe?token=3870a715c186351033c95f5fe6947a21|https://sub3.smallstrawberry.com/api/v1/client/subscribe?token=do9qzecas3uas1bofa1plw5sx2j2h7ui"
config="https://gitlab.com/silverzhaojr/mess/-/raw/main/singbox-config/config.ini"
exclude="25"

mkdir -p sb-tmp && cd sb-tmp && \
curl --max-time 10 -o sub.json \
     --url-query "url=${suburl}" \
     --url-query "config=${config}" \
     --url-query "exclude=${exclude}" \
     "${server}/sub?target=singbox&insert=false&emoji=false&list=false&xudp=false&udp=false&tfo=false&expand=false&scv=false&fdn=false" && \
sing-box format -c sub.json | sed -n '/"outbounds":/,/^  "[a-z]/p' | sed '$d' > o.json && \
echo '{' > outbounds.json && sed  's/^  ],$/  ]/' o.json >> outbounds.json && echo '}' >> outbounds.json && \
patch -i "${prefix_dir}/outbounds.patch" && \
sing-box merge config.json -c "${prefix_dir}/base.json" -c outbounds.json && \
sing-box check -c config.json
